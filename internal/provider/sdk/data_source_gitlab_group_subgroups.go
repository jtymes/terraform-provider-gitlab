package sdk

import (
	"context"
	"fmt"

	"github.com/hashicorp/terraform-plugin-log/tflog"
	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"gitlab.com/gitlab-org/api/client-go"
)

var _ = registerDataSource("gitlab_group_subgroups", func() *schema.Resource {
	return &schema.Resource{
		Description: `The ` + "`gitlab_group_subgroups`" + ` data source allows to get subgroups of a group.

**Upstream API**: [GitLab REST API docs](https://docs.gitlab.com/ee/api/groups.html#list-a-groups-subgroups)`,

		ReadContext: dataSourceGitlabGroupSubgroupsRead,
		Schema: map[string]*schema.Schema{
			"group_id": {
				Description: "The ID of the group.",
				Type:        schema.TypeInt,
				Required:    true,
			},
			"skip_groups": {
				Description: "Skip the group IDs passed.",
				Type:        schema.TypeList,
				Computed:    true,
				Optional:    true,
				Elem:        &schema.Schema{Type: schema.TypeInt},
			},
			"all_available": {
				Description: "Show all the groups you have access to.",
				Type:        schema.TypeBool,
				Computed:    true,
				Optional:    true,
			},
			"search": {
				Description: "Return the list of authorized groups matching the search criteria.",
				Type:        schema.TypeString,
				Computed:    true,
				Optional:    true,
			},
			"order_by": {
				Description: "Order groups by name, path or id.",
				Type:        schema.TypeString,
				Computed:    true,
				Optional:    true,
			},
			"sort": {
				Description: "Order groups in asc or desc order.",
				Type:        schema.TypeString,
				Computed:    true,
				Optional:    true,
			},
			"statistics": {
				Description: "Include group statistics (administrators only).",
				Type:        schema.TypeBool,
				Computed:    true,
				Optional:    true,
			},
			"with_custom_attributes": {
				Description: "Include custom attributes in response (administrators only).",
				Type:        schema.TypeBool,
				Computed:    true,
				Optional:    true,
			},
			"owned": {
				Description: "Limit to groups explicitly owned by the current user.",
				Type:        schema.TypeBool,
				Computed:    true,
				Optional:    true,
			},
			"min_access_level": {
				Description: "Limit to groups where current user has at least this access level.",
				Type:        schema.TypeString,
				Computed:    true,
				Optional:    true,
			},
			"subgroups": {
				Description: "Subgroups of the parent group.",
				Type:        schema.TypeList,
				Computed:    true,
				Elem: &schema.Resource{
					Schema: datasourceSchemaFromResourceSchema(gitlabGroupSchema(), nil, nil),
				},
			},
		},
	}
})

func dataSourceGitlabGroupSubgroupsRead(ctx context.Context, d *schema.ResourceData, meta interface{}) diag.Diagnostics {
	client := meta.(*gitlab.Client)

	tflog.Info(ctx, "Gitlab group subgroups")

	groupIDData, groupIDOk := d.GetOk("group_id")
	if !groupIDOk {
		return diag.Errorf("group_id is not valid")
	}

	options := gitlab.ListSubGroupsOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 20,
			Page:    1,
		},
	}
	if data, ok := d.GetOk("skip_groups"); ok {
		skipGroups := intListToIntSlice(data.([]interface{}))
		options.SkipGroups = skipGroups
	}

	var subgroups []*gitlab.Group
	for options.Page != 0 {
		// List subgroups
		paginatedSubgroups, resp, err := client.Groups.ListSubGroups(groupIDData.(int), &options, gitlab.WithContext(ctx))
		if err != nil {
			return diag.FromErr(err)
		}

		subgroups = append(subgroups, paginatedSubgroups...)
		options.Page = resp.NextPage
	}

	d.SetId(fmt.Sprintf("%d", groupIDData))
	if err := d.Set("subgroups", flattenSubgroupsForState(subgroups)); err != nil {
		return diag.Errorf("Failed to set subgroups to state: %v", err)
	}

	return nil
}

func flattenSubgroupsForState(subgroups []*gitlab.Group) (values []map[string]interface{}) {
	for _, group := range subgroups {
		values = append(values, gitlabGroupToStateMap(group))
	}
	return values
}
